# Funpodium Captcha Client SDK for iOS

Funpodium Captcha Client SDK for iOS is a iOS library for FP Captcha Service users to
integrate Captcha functions to iOS native app.



Instructions
-------

* [Setup](#setup)
* [How to Use it](#how-to-use-it)
    * [Start a Challenge](#Start-a-Challenge)
* [R8 / ProGuard](r8-proguard)

----
Setup
-----
Initialized SDK before using. Add below code to your ViewController

```Groovy
#import FPCaptcha
```

How to Use it
----
Before starting a Captcha challenge. App need to initialize `FPCaptcha` first. This step only need
to be trigger once

```swift
/* initial FPCaptcha in your Application class or Activity class */
FunCaptcha.shared.config(serverType: .stage, language: lan, device: Device(language: lan, ip: "1.1.1.1", userAgent: nil, domain: nil))
```

Start a Challenge
-------
After receive a challenge request which means app get a  `challengeId`  of a challenge. First,
create a `CaptchaViewController` and setup callback to receive challenge result.

```swift
CaptchaViewControllerBuilder().setupUuid(uuid: uuid).setupCaptchaCallback(captchaCallback: { (result) in
    switch result{
    case .success(let code):
        print("code: \(String(describing: code))")
    case .failure(let error):
        print("code: \(error.code), message: \(error.message)")
    }
})
```

Then display dialogVC.

There are 2 way to display dialogVC.  
`show()` - Display dialogVC immediatelly. Challenge will be prepared after that.
`showAfterPrepared` - Prepared challenge then show dialogVC once required challenge information is
prepared.

```swift
    dialogVC.show()
    dialogVC.showAfterPrepared();
```
