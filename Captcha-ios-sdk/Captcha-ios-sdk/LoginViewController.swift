//
//  LoginViewController.swift
//  Captcha-ios-sdk
//
//  Created by Jack Din on 2021/9/27.
//

import UIKit
import FPCaptcha

class LoginViewController: UIViewController {

    enum ButtonType: Int {
        case normalAccount = 1000
        case bypassAccount
        case showType
        case showAfterPreparedType
      
        /*
         accessKey: 9c11df3ef0cc48ea06470a2abe49680d
         secretKey: fee926bea116e20967073c10acc57fc5
         這組是strict, refresh次數5、retry次數10、cost 1~500,000、ttl 500秒

         accessKey: cd9e441ecb9dfcf20868a18b5bc13b00
         secretKey: 1e21156b983a3a8e7acc7798799fb682
         這組是bypass, refresh次數5、retry次數5、cost 1~500,000、ttl 500秒
         */
        var account: String{
            switch self {
            case .normalAccount:
                return "9c11df3ef0cc48ea06470a2abe49680d"
            default:
                return "cd9e441ecb9dfcf20868a18b5bc13b00"
            }
        }
    
        var password: String{
            switch self {
            case .normalAccount:
                return "fee926bea116e20967073c10acc57fc5"
            default:
                return "1e21156b983a3a8e7acc7798799fb682"
            }
        }
    }
    
    var account: String = "9c11df3ef0cc48ea06470a2abe49680d"
    var password: String = "fee926bea116e20967073c10acc57fc5"
    var dialogVC: CaptchaViewController!
    
    lazy var janeImageView: UIImageView = {
        let janeImageView = UIImageView()
        janeImageView.translatesAutoresizingMaskIntoConstraints = false
        janeImageView.contentMode = .scaleAspectFit
        janeImageView.image = UIImage(named: "456", in: Bundle(for: LoginViewController.self), compatibleWith: nil)
        return janeImageView
    }()
    
    lazy var normalButton: UIButton = {
        let normalButton = UIButton()
        normalButton.translatesAutoresizingMaskIntoConstraints = false
        normalButton.setTitle("Normal", for: .normal)
        normalButton.setTitleColor(.black, for: .normal)
        normalButton.backgroundColor = .lightGray
        normalButton.setTitleColor(.red, for: .highlighted)
        normalButton.tag = ButtonType.normalAccount.rawValue
        normalButton.addTarget(self, action: #selector(setupAccount(sender:)), for: .touchUpInside)
        return normalButton
    }()

    lazy var bypassButton: UIButton = {
        let bypassButton = UIButton()
        bypassButton.translatesAutoresizingMaskIntoConstraints = false
        bypassButton.setTitle("Bypass", for: .normal)
        bypassButton.setTitleColor(.black, for: .normal)
        bypassButton.backgroundColor = .lightGray
        bypassButton.setTitleColor(.red, for: .highlighted)
        bypassButton.tag = ButtonType.bypassAccount.rawValue
        bypassButton.addTarget(self, action: #selector(setupAccount(sender:)), for: .touchUpInside)
        return bypassButton
    }()
    
    lazy var showButton: UIButton = {
        let showButton = UIButton()
        showButton.translatesAutoresizingMaskIntoConstraints = false
        showButton.setTitle("Login with show", for: .normal)
        showButton.setTitleColor(.white, for: .normal)
        showButton.backgroundColor = .darkGray
        showButton.setTitleColor(.lightGray, for: .highlighted)
        showButton.tag = ButtonType.showType.rawValue
        showButton.addTarget(self, action: #selector(popUpCaptchaView(sender:)), for: .touchUpInside)
        return showButton
    }()

    lazy var showAfterPreparedButton: UIButton = {
        let showAfterPreparedButton = UIButton()
        showAfterPreparedButton.translatesAutoresizingMaskIntoConstraints = false
        showAfterPreparedButton.setTitle("Login by show after prepared", for: .normal)
        showAfterPreparedButton.setTitleColor(.white, for: .normal)
        showAfterPreparedButton.backgroundColor = .darkGray
        showAfterPreparedButton.setTitleColor(.lightGray, for: .highlighted)
        showAfterPreparedButton.tag = ButtonType.showAfterPreparedType.rawValue
        showAfterPreparedButton.addTarget(self, action: #selector(popUpCaptchaView(sender:)), for: .touchUpInside)
        return showAfterPreparedButton
    }()

    lazy var cancelButton: UIButton = {
        let cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.setTitle("Cancel Alert", for: .normal)
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.backgroundColor = .darkGray
        cancelButton.setTitleColor(.lightGray, for: .highlighted)
        cancelButton.addTarget(self, action: #selector(cancelPop(sender:)), for: .touchUpInside)
        return cancelButton
    }()

    var networkSession: URLSession{URLSession.init(configuration: .default)}

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupLayout()
    }
}

extension LoginViewController{
    func setupLayout() {
        self.view.addSubview(janeImageView)
        self.view.addSubview(normalButton)
        self.view.addSubview(bypassButton)
        self.view.addSubview(showButton)
        self.view.addSubview(showAfterPreparedButton)
//        self.view.addSubview(cancelButton)
        self.view.backgroundColor = .white
        
        // setup janeImageView
        janeImageView.bottomAnchor.constraint(equalTo: normalButton.topAnchor, constant: 12).isActive = true
        janeImageView.widthAnchor.constraint(equalToConstant: view.frame.height - view.center.y - 40).isActive = true
        janeImageView.heightAnchor.constraint(equalToConstant: view.frame.height - view.center.y - 40).isActive = true
        janeImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        janeImageView.alpha = 0
        // setup normalButton
        normalButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        normalButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        normalButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        normalButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        // setup bypassButton
        bypassButton.topAnchor.constraint(equalTo: normalButton.bottomAnchor, constant: 12).isActive = true
        bypassButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        bypassButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        bypassButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        // setup loginButton
        showButton.topAnchor.constraint(equalTo: bypassButton.bottomAnchor, constant: 32).isActive = true
        showButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        showButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        showButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        // setup loginButton
        showAfterPreparedButton.topAnchor.constraint(equalTo: showButton.bottomAnchor, constant: 12).isActive = true
        showAfterPreparedButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        showAfterPreparedButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        showAfterPreparedButton.heightAnchor.constraint(equalToConstant: 22).isActive = true
        // setup loginButton
//        cancelButton.topAnchor.constraint(equalTo: showAfterPreparedButton.bottomAnchor, constant: 12).isActive = true
//        cancelButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
//        cancelButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
//        cancelButton.heightAnchor.constraint(equalToConstant: 22).isActive = true

        UIView.animate(withDuration: 3) {
            self.janeImageView.alpha = 1
        }
    }
    
    func showToast(message : String, font: UIFont) {
        DispatchQueue.main.async {
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - self.view.frame.size.width/4, y: self.view.frame.size.height-100, width: self.view.frame.size.width/2, height: 88))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.numberOfLines = 0
            toastLabel.textColor = UIColor.white
            toastLabel.font = font
            toastLabel.textAlignment = .center;
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
                 toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    
    @objc func popUpCaptchaView(sender: UIButton) {
        
        getChallengeUuid(completed: { (uuid) in
            let lan: String = "zh" ?? "en"
            FunCaptcha.shared.config(serverType: .stage, language: lan, device: Device(language: lan, ip: "1.1.1.1", userAgent: nil, domain: nil))
            
            DispatchQueue.main.async {[weak self] in
                guard let `self` = self else {return}
                `self`.dialogVC = CaptchaViewControllerBuilder().setupUuid(uuid: uuid)
                    .setupCaptchaCallback(captchaCallback: { (result) in
                    switch result{
                    case .success(let code):
                        print("code: \(String(describing: code))")
                        `self`.showToast(message: "code: \(String(describing: code))", font: UIFont.systemFont(ofSize: 14))
                    case .failure(let error):
                        print("code: \(error.code), message: \(error.message)")
                        `self`.showToast(message: "code: \(error.code), message: \(error.message)", font: UIFont.systemFont(ofSize: 14))
                    }
                }).build()
                
                if sender.tag == ButtonType.showType.rawValue{
                    `self`.dialogVC.show()
                }else{
                    `self`.dialogVC.showAfterPrepared()
                }
            }
        })
    }

    @objc func cancelPop(sender: UIButton){
        dialogVC.cancelCaptchaDialogView()
    }
    
    @objc func setupAccount(sender: UIButton){
        account = ButtonType(rawValue: sender.tag)?.account ?? ""
        password = ButtonType(rawValue: sender.tag)?.password ?? ""
    }
}

extension LoginViewController{
    func getChallengeUuid(completed:  @escaping (String) -> Void) {
        guard let component = URLComponents(string: "https://kongstagingbetway178-dev.gamealiyun.com/api/Captcha/RequestCaptchaChallengeId?api-version=1.0&Brand=BETWAY&Platform=IOS") else {return}
        guard let configUrl = component.url?.absoluteURL else {return}
        var request = URLRequest(url: configUrl)

        request.allHTTPHeaderFields = ["Culture": "ZH-CN"]
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]
        request.httpMethod = "POST"
        request.httpBody = ChallengeRequestData(accessKey: account, secretKey: password).body

        self.networkSession.dataTask(with: request) { (data, response, error) in
            let dic: [String: Any] = try! JSONSerialization.jsonObject(with: data ?? Data(), options: .allowFragments) as! [String : Any]
            let uuid: String = dic["challengeUuid"] as! String
            completed(uuid)
        }.resume()
    }
}

class ChallengeRequestData: Encodable {
    let accessKey: String
    let secretKey: String
    let serverIp: String

    
    init(accessKey: String, secretKey: String) {
        self.accessKey = accessKey
        self.secretKey = secretKey
        self.serverIp = "1.1.1.1"
    }
}

extension ChallengeRequestData{
    var body: Data?{
        try? JSONEncoder().encode([
            "accessKey": accessKey,
            "secretKey": secretKey,
            "serverIp": serverIp
        ])
    }
}
