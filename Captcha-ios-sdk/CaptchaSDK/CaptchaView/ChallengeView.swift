//
//  CaptchaView.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/27.
//

import UIKit

protocol ChallengViewDelegate: AnyObject {
    func captchaView (captchaView: CaptchaView, onAction: CGPoint, cost: Double)
}

class CaptchaView: UIView {
    var originalImageSize: CGSize?
    var scaledImageSize: CGSize?
    var startTime: TimeInterval = Date().timeIntervalSince1970
    private var endTime: TimeInterval{
        Date().timeIntervalSince1970
    }
    var captchaViewWording: CaptchaWordingProtocol?
    weak var delegate: ChallengViewDelegate?
    var templateLeadingConstraint: NSLayoutConstraint?
    var templateTopConstraint: NSLayoutConstraint?
    var templateWidthConstraint: NSLayoutConstraint?
    var templateHeighConstraint: NSLayoutConstraint?
    lazy var issueImageView: UIImageView = {
        let issueImageView = UIImageView()
        issueImageView.backgroundColor = UIColor(hexString: "#f5f3f4")
        issueImageView.translatesAutoresizingMaskIntoConstraints = false
        issueImageView.contentMode = .center
        return issueImageView
    }()
    
    lazy var issueLabel: UILabel = {
        let issueLabel = UILabel()
        issueLabel.numberOfLines = 0
        issueLabel.textAlignment = .center
        issueLabel.backgroundColor = .clear
        issueLabel.textColor = UIColor(hexString: "#9b999a")
        issueLabel.translatesAutoresizingMaskIntoConstraints = false
        issueLabel.font = .systemFont(ofSize: 16)
        return issueLabel
    }()
    
    lazy var templateImageView: UIImageView = {
        let templateImageView = UIImageView()
        templateImageView.translatesAutoresizingMaskIntoConstraints = false
        templateImageView.contentMode = .scaleAspectFit
        return templateImageView
    }()
    lazy var puzzleImageView: UIImageView = {
        let puzzleImageView = UIImageView()
        puzzleImageView.translatesAutoresizingMaskIntoConstraints = false
        puzzleImageView.contentMode = .scaleAspectFit
        return puzzleImageView
    }()
    
    lazy var statusImageView: UIImageView = {
        let statusImageView = UIImageView()
        statusImageView.translatesAutoresizingMaskIntoConstraints = false
        statusImageView.contentMode = .center
        return statusImageView
    }()

    lazy var statusLabel: UILabel = {
        let statusLabel = UILabel()
        statusLabel.numberOfLines = 0
        statusLabel.textAlignment = .left
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        statusLabel.backgroundColor = .clear
        statusLabel.font = .systemFont(ofSize: 12)
        return statusLabel
    }()

    lazy var statusContentView: UIView = {
        let statusContentView = UIView()
        statusContentView.translatesAutoresizingMaskIntoConstraints = false
        return statusContentView
    }()
    var puzzleScaleRatio: CGFloat = 0
    
    enum ChartViewStatus {
        case refresh
        case networkFail
        case showChart
    }
    
    private var chartViewStatus: ChartViewStatus = .refresh{
        didSet{
            refreshChartView()
        }
    }
    
    enum StatusBarType {
        case start
        case update
    }
    
    public init(){
        super.init(frame: .zero)

        self.addSubview(issueImageView)
        self.addSubview(puzzleImageView)
        self.addSubview(templateImageView)
        self.addSubview(statusContentView)
        statusContentView.addSubview(statusImageView)
        statusContentView.addSubview(statusLabel)
        templateWidthConstraint = templateImageView.widthAnchor.constraint(equalToConstant: 0)
        templateHeighConstraint = templateImageView.heightAnchor.constraint(equalToConstant: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configCaptchaWording(captchaViewWording: CaptchaWordingProtocol) {
        self.captchaViewWording = captchaViewWording
    }
}

extension CaptchaView{
    public func layoutUI(ratio: CGFloat, puzzleImage: UIImage, templateImage: UIImage){
        //setup puzzleImageView 拼圖底版
        puzzleImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        puzzleImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        puzzleImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        puzzleImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        puzzleImageView.image = puzzleImage
        
        //setup templateImageView
        let scaledTemplateWidth = templateImage.size.width  * ratio
        templateImageView.leadingAnchor.constraint(equalTo: puzzleImageView.leadingAnchor).isActive = true
        templateImageView.topAnchor.constraint(equalTo: puzzleImageView.topAnchor).isActive = true
        templateHeighConstraint?.constant = scaledTemplateWidth
        templateWidthConstraint?.constant = scaledTemplateWidth
        templateHeighConstraint?.isActive = true
        templateWidthConstraint?.isActive = true
        templateImageView.image = templateImage
        setupGuesture()
        
        //setup statusContentView
        statusContentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        statusContentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        statusContentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        statusContentView.heightAnchor.constraint(equalToConstant: 30).isActive = true

        //setup statusImageView
        statusImageView.leadingAnchor.constraint(equalTo: statusContentView.leadingAnchor).isActive = true
        statusImageView.topAnchor.constraint(equalTo: statusContentView.topAnchor).isActive = true
        statusImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        statusImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true

        //setup statusLabel
        statusLabel.leadingAnchor.constraint(equalTo: statusImageView.trailingAnchor).isActive = true
        statusLabel.topAnchor.constraint(equalTo: statusContentView.topAnchor).isActive = true
        statusLabel.bottomAnchor.constraint(equalTo: statusContentView.bottomAnchor).isActive = true
        statusLabel.trailingAnchor.constraint(equalTo: statusContentView.trailingAnchor).isActive = true
        
        //setup issueImageView 顯示網路連線底圖
        issueImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        issueImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        issueImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        issueImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        issueImageView.addSubview(issueLabel)
        issueLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        issueLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        issueLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 30).isActive = true
    }
    
    func setupChartViewStatus(chartViewStatus: ChartViewStatus) {
        self.chartViewStatus = chartViewStatus
    }

    public func resetTemplateImageViewLocation(){
        self.templateImageView.frame.origin = puzzleImageView.frame.origin
    }

    private func refreshChartView() {
        DispatchQueue.main.async {[weak self] in
            guard let `self` = self else {return}
            `self`.issueLabel.isHidden = `self`.chartViewStatus != .networkFail
            `self`.puzzleImageView.isHidden = `self`.chartViewStatus != .showChart
            `self`.templateImageView.isHidden = `self`.chartViewStatus != .showChart
            let abnormal: String = `self`.captchaViewWording?.abnormal ?? Wording.abnormal
            switch `self`.chartViewStatus {
            case .networkFail:
                `self`.issueImageView.image = UIImage(named: "Internet Error", in: Bundle(for: type(of: self)), compatibleWith: nil)
                `self`.issueLabel.text = abnormal
            case .refresh:
                `self`.issueImageView.image = UIImage(named: "Refresh", in: Bundle(for: type(of: self)), compatibleWith: nil)
            case .showChart:
                break
            }
        }
    }
    
    private func displayStatusContentView(status: StatusBarType) {
        statusContentView.alpha = status == StatusBarType.start ? 0.8 : 0
    }
    
    func judgeResultAnimate(result: Result<DisplayMessageProtocol?, CaptchaFail>, completed: (() -> Void)?) {
        DispatchQueue.main.async {[weak self] in
            guard let `self` = self else {return}
            `self`.displayStatusContentView(status: .start)
            `self`.templateImageView.isUserInteractionEnabled = false
            switch result{
            case .success(_):
                `self`.statusImageView.image = UIImage(named: "Success", in: Bundle(for: type(of: `self`)), compatibleWith: nil)
                `self`.statusLabel.textColor = ColorPatten.captchaSuccessText.color
                `self`.statusContentView.backgroundColor =  ColorPatten.captchaSuccessStatusView.color
                `self`.statusLabel.text = CaptchaDialogViewModel.challengeInfo?.currentLocale?.success
            case .failure(let error):
                `self`.resetTemplateImageViewLocation()
                `self`.statusImageView.image = UIImage(named: "Fail", in: Bundle(for: type(of: `self`)), compatibleWith: nil)
                `self`.statusLabel.textColor = ColorPatten.captchaFailText.color
                `self`.statusContentView.backgroundColor =  ColorPatten.captchaViewFailStatusView.color
                `self`.statusLabel.text = error.message
                `self`.templateImageView.isUserInteractionEnabled = true
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                `self`.displayStatusContentView(status: .update)
            }
            completed?()
        }
    }
    
    func setupIssueLabelMessage(message: String) {
        DispatchQueue.main.async {[weak self] in
            guard let `self` = self else {return}
            `self`.issueLabel.isHidden = false
            `self`.issueLabel.text = message
        }
    }
    
    private func setupGuesture() {
        // add template listener
        let pan = UIPanGestureRecognizer(target:self,action:#selector(pan(_:)))
        pan.minimumNumberOfTouches = 1
        pan.maximumNumberOfTouches = 1
        self.templateImageView.isUserInteractionEnabled = true
        self.templateImageView.addGestureRecognizer(pan)
        self.resetTemplateImageViewLocation()
    }
    
//     UIImage Listener
    @objc private func pan(_ recognizer:UIPanGestureRecognizer) {
        if recognizer.state == UIGestureRecognizer.State.began {
            startTime = Date().timeIntervalSince1970
        }
        let translation = recognizer.translation(in: recognizer.view)
        var changeX = (recognizer.view?.frame.origin.x)! + translation.x
        var changeY = (recognizer.view?.frame.origin.y)! + translation.y
        let templateHeight = recognizer.view?.frame.height
        let templateWidth = recognizer.view?.frame.width
        let puzzleHeight = puzzleImageView.frame.height
        let puzzleWidth = puzzleImageView.frame.width
        
        if (changeX < 0){
            changeX = 0
        }
        if (changeY < 0){
            changeY = 0
        }
        if((changeX + templateWidth!) > puzzleWidth){
            changeX = puzzleWidth - templateWidth!
        }
        if((changeY + templateHeight!) > puzzleHeight){
            changeY = puzzleHeight - templateHeight!
        }
        
        recognizer.view?.frame.origin = CGPoint(x: changeX, y: changeY)
        recognizer.setTranslation(CGPoint.zero, in: recognizer.view)
        
        if recognizer.state == UIGestureRecognizer.State.ended{
            self.delegate?.captchaView(captchaView: self, onAction: CGPoint(x: changeX, y: changeY), cost: Double(endTime - startTime) * 1000.0)
        }
    }
}
