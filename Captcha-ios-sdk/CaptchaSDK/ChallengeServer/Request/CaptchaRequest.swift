//
//  CaptchaRequest.swift
//  Captcha
//
//  Created by Jack Din on 2021/8/25.
//

import Foundation
import AdSupport
import WebKit

protocol URLPath {
    var path: [String: String] {get}
}

protocol HTTPBody {
    var body: Data? {get}
}

class ChartInfoRequestData: Encodable {
    let challengeUuid: String
    let applicationLanguage: String? = FunCaptcha.shared.language
    var device: Device
    
    init(challengeUuid: String, device: Device) {
        self.challengeUuid = challengeUuid
        self.device = device
    }
}

extension ChartInfoRequestData: URLPath, HTTPBody{
    var path: [String: String] {
        ["challengeUuid": challengeUuid]
    }
    var body: Data?{
        try? JSONEncoder().encode(self)
    }
}

class JudgementRequestData: Encodable {
    let challengeUUID: String
    let activity: Activity
    let device: Device
    let applicationLanguage: String? = FunCaptcha.shared.language

    enum CodingKeys: String, CodingKey {
        case applicationLanguage
        case challengeUUID = "challengeUuid"
        case activity, device
    }
    
    // MARK: - Activity
    struct Activity: Encodable {
        let answers: Answers
        let cost: Double
    }

    // MARK: - Answers
    struct Answers: Encodable {
        let x, y: Int
    }

    // MARK: - Device
    struct Device: Encodable {
        let ip, language, userAgent, uuid: String?
        let domain: String?
    }
    
    init(challengeUUID: String, activity: Activity, device: Device) {
        self.challengeUUID = challengeUUID
        self.activity = activity
        self.device = device
    }
}

extension JudgementRequestData: HTTPBody{
    var body: Data? {
        try? JSONEncoder().encode(self)
    }
}

//class RefreshChartRequestData: Encodable {
//    var challengeUuid: String
//    
//    var language: String?
//    var ip: String?
//    var domain: String?
//    var userAgent: String?
//    var udid: String?
//    var device: Device
//
//    init(challengeUuid: String, device: Device) {
//        self.challengeUuid = challengeUuid
//        self.language = device.language
//        self.ip = device.ip
//        self.domain = device.domain
//        self.userAgent = device.userAgent
//        self.udid = device.udid
//        
//    }
//}
//
//extension RefreshChartRequestData: URLPath, HTTPBody{
//    var path: [String: String] {
//        ["challengeUuid": challengeUuid]
//    }
//    var body: Data?{
//        try? JSONEncoder().encode(self)
//    }
//}

