//
//  CaptchaResponseData.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/8.
//

import Foundation

public class ChartInfoResponseData: NSObject, UnifyInitProtocol, Decodable, PuzzleViewProtocol {
    let data: CaptchaModels.ChartInfoData
    
    required init(data: CaptchaModels.ChartInfoData) {
        self.data = data
    }
    
    var challengeUUID: String? {self.data.challengeUUID}
    var chartID: Int {self.data.chartID ?? 0}
    var chartURI: String? {self.data.chartURI}
    var mode: String? {self.data.mode}
    var currentLocale: CurrentLocale? {CurrentLocale(data: self.data.currentLocale)}
    var deviceID: Int {self.data.deviceID ?? 0}
    var keyURI: String? {self.data.keyURI}
    var message: String? {self.data.message}
    var refreshCount: Int {self.data.refreshCount ?? 0}
    var retryCount: Int {self.data.retryCount ?? 0}
    var shuffleMatrix: [Int] {self.data.shuffleMatrix ?? []}
    var code: Int {self.data.code ?? ResponseCode.success.rawValue}
    var createTime: String? {self.data.createTime}

    // MARK: - CurrentLocale
    public class CurrentLocale: Decodable, CaptchaWordingProtocol {
        let data: CaptchaModels.ChartInfoData.CurrentLocale?
        
        required init(data: CaptchaModels.ChartInfoData.CurrentLocale?) {
            self.data = data
        }

        var fail: String? {self.data?.fail}
        var abnormal: String? {self.data?.abnormal}
        var provide: String? {self.data?.provide}
        var success: String? {self.data?.success}
        var refresh: String? {self.data?.refresh}
        var currentLocaleDescription: String? {self.data?.currentLocaleDescription}
        var info: String? {self.data?.info}
    }
}

class JudgementResponseData: NSObject, UnifyInitProtocol, Decodable, DisplayMessageProtocol {
    let data: CaptchaModels.JudgementData
    
    required init(data: CaptchaModels.JudgementData) {
        self.data = data
    }
    
    var challengeUUID: String? {self.data.challengeUUID}
    var mode: String? {self.data.mode}
    var message: String? {self.data.message}
    var createTime: String? {self.data.createTime}
    var code: Int {self.data.code ?? ResponseCode.success.rawValue}
}

class RefreshChartResponseData: NSObject, UnifyInitProtocol, Decodable, PuzzleViewProtocol {
    let data: CaptchaModels.RefreshChartData
    
    required init(data: CaptchaModels.RefreshChartData) {
        self.data = data
    }
    
    var challengeUUID: String? {self.data.challengeUUID}
    var chartURI: String? {self.data.chartURI}
    var keyURI: String? {self.data.keyURI}
    var shuffleMatrix: [Int] {self.data.shuffleMatrix ?? []}
    var refreshCount: Int {self.data.refreshCount ?? 0}
    var mode: String? {self.data.mode}
    var code: Int {self.data.code ?? ResponseCode.success.rawValue}
    var createTime: String? {self.data.createTime}
}

public class CaptchaErrorResponseData: Decodable, UnifyInitProtocol, DisplayMessageProtocol{
    let data: CaptchaErrorData
    
    required init(data: CaptchaErrorData) {
        self.data = data
    }
    
    var path: String? {self.data.path}
    var status: String? {self.data.status}
    var message: String? {self.data.message}
    var code: Int {self.data.code }
}

protocol PuzzleViewProtocol {
    var challengeUUID: String? {get}
    var chartURI: String? {get}
    var keyURI: String? {get}
    var shuffleMatrix: [Int] {get}
}

protocol CaptchaWordingProtocol {
    var fail: String? {get}
    var abnormal: String? {get}
    var provide: String? {get}
    var success: String? {get}
    var refresh: String? {get}
    var currentLocaleDescription: String? {get}
    var info: String? {get}
}

protocol DisplayMessageProtocol {
    var message: String? {get}
    var code: Int {get}
}
