//
//  UnifyOutput.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/9.
//

import Foundation

protocol UnifyInitProtocol{
    associatedtype T: Decodable
    init( data: T )
}

