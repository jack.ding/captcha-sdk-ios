//
//  CaptchaModels.swift
//  Captcha
//
//  Created by Jack Din on 2021/8/25.
//

import Foundation
import AdSupport

struct CaptchaModels {
    struct ChartInfoData: Decodable {
        let challengeUUID: String?
        let chartID: Int?
        let chartURI: String?
        let mode: String?
        let currentLocale: CurrentLocale?
        let deviceID: Int?
        let keyURI, message: String?
        let refreshCount, retryCount: Int?
        let shuffleMatrix: [Int]?
        let code: Int?
        let createTime: String?
        
        enum CodingKeys: String, CodingKey {
            case challengeUUID = "challengeUuid"
            case chartID = "chartId"
            case chartURI = "chartUri"
            case currentLocale
            case mode
            case deviceID = "deviceId"
            case keyURI = "keyUri"
            case message, refreshCount, retryCount, shuffleMatrix
            case code, createTime
        }

        // MARK: - CurrentLocale
        struct CurrentLocale: Decodable {
            let fail, abnormal, provide, success: String?
            let refresh, currentLocaleDescription, info: String?

            enum CodingKeys: String, CodingKey {
                case fail, abnormal, provide, success, refresh
                case currentLocaleDescription = "description"
                case info
            }
        }
    }

    struct JudgementData: Decodable {
        let challengeUUID, mode: String?
        let code: Int?
        let message, createTime: String?
        
        enum CodingKeys: String, CodingKey {
            case challengeUUID = "challengeUuid"
            case mode, code, message, createTime
        }
    }

    struct RefreshChartData: Decodable {
        let challengeUUID: String?
        let chartURI: String?
        let keyURI: String?
        let shuffleMatrix: [Int]?
        let refreshCount: Int?
        let mode: String?
        let code: Int?
        let createTime: String?

        enum CodingKeys: String, CodingKey {
            case challengeUUID = "challengeUuid"
            case chartURI = "chartUri"
            case keyURI = "keyUri"
            case shuffleMatrix, refreshCount, mode
            case code, createTime
        }
    }
}

public struct CaptchaErrorData: Decodable, Error{
    let path, status: String?
    let message: String
    let code: Int
}


/*
 // MARK: - Device
    struct Device: Encodable {
     let ip, language, userAgent, uuid: String?
     let domain: String?
 }
 */
public struct Device: Encodable {
    let udid: String? = ASIdentifierManager.shared().advertisingIdentifier.uuidString
    let language: String?
    var ip: String?
    var domain: String?
    var userAgent: String?
    
    public init(language: String?, ip: String?, userAgent: String?, domain: String?) {
        self.ip = ip
        self.userAgent = userAgent
        self.language = language
        self.domain = domain
    }
}

