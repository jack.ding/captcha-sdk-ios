//
//  CaptchaApiManager.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/1.
//

import UIKit

protocol CaptchaProtocol {
    static var language: String { get }
    static var captchaURL: String { get }
    static var networkSession: URLSession { get }
}

class CaptchaAPI: NSObject {
    static var networkSession: URLSession {URLSession(configuration: .default)}
    static var language: String {FunCaptcha.shared.language}
    static var captchaURL: String {
        switch FunCaptcha.shared.serverType{
        case .dev, .stage:
            return "https://tkscp-captcha.funpo.com:2041/captcha-pro-service"
        case .softlaunch:
            return "https://tkslcp-captcha.funpo.com:2041/captcha-pro-service"
        case .production:
            return "https://tkpcp-captcha.funpo.com:2041/captcha-pro-service"
        }
    }
    
    override init() {
    }
}

extension CaptchaAPI: CaptchaProtocol{}

extension CaptchaProtocol where Self: CaptchaAPI{

    static func getChartInfo(request: ChartInfoRequestData, completed: @escaping((Result<ChartInfoResponseData?, CaptchaFail>) -> Void)){
        let path = "/api/v1.0/challenge/\(request.challengeUuid)"
        networkCommunication(urlString: path, httpMethod: .Post(Data: request.body), returnType: CaptchaModels.ChartInfoData.self) { (result) in
            switch result{
            case .success(let object):
                guard let _object = object else {
                    return
                }
                completed(.success(ChartInfoResponseData(data: _object)))
            case .failure(let error):
                completed(.failure(error))
            }
        }
    }
    
    static func judgeAnswer(request: JudgementRequestData, completed: @escaping((Result<JudgementResponseData?, CaptchaFail>) -> Void)){
        let path = "/api/v1.0/judgement"
        networkCommunication(urlString: path, httpMethod: .Post(Data: request.body), returnType: CaptchaModels.JudgementData.self) { (result) in
            switch result{
            case .success(let object):
                guard let _object = object else {
                    return
                }
                completed(.success(JudgementResponseData(data: _object)))
            case .failure(let error):
                completed(.failure(error))
            }
        }
    }
    
    static func refreshChart(request: ChartInfoRequestData, completed: @escaping((Result<RefreshChartResponseData?, CaptchaFail>) -> Void)){
        let path = "/api/v1.0/chart/\(request.challengeUuid)"
        networkCommunication(urlString: path, httpMethod: .Post(Data: request.body), returnType: CaptchaModels.RefreshChartData.self) { (result) in
            switch result{
            case .success(let object):
                guard let _object = object else {
                    return
                }
                completed(.success(RefreshChartResponseData(data: _object)))
            case .failure(let error):
                completed(.failure(error))
            }
        }
    }
}

extension CaptchaProtocol{
    static func networkCommunication<T>(urlString: String, httpMethod: HttpMethod, returnType: T.Type, complete: @escaping ((Result<T?, CaptchaFail>) -> Void)) where T: Decodable{
        guard let component = URLComponents(string: self.captchaURL + urlString) else {
            return
        }
        
        guard let configUrl = component.url?.absoluteURL else {
            return
        }
        
        var request = URLRequest(url: configUrl)
        request.allHTTPHeaderFields = ["Culture": self.language]
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]

        switch httpMethod {
        case .Post(Method: let method, Data: let data):
            request.httpMethod = method
            request.httpBody = data
        }
        self.networkSession.invalidateAndCancel()
        self.networkSession.dataTask(with: request) { (data, response, error) in
            print("//===================================//")
            dump(data?.dataToString())
            print("//===================================//")
            
            guard let _error = error else {
                if let _data = data {
                    self.decodeResponse(data: _data, returnType: returnType, complete: complete)
                    return
                }
                return
            }
            complete(.failure(CaptchaFail(code: (_error as NSError).code , message: (_error as NSError).description)))
        }.resume()
    }
    
    static func decodeResponse<T: Decodable>(data: Data, returnType: T.Type, complete: @escaping ((Result<T?, CaptchaFail>) -> Void)){
        let decoder = JSONDecoder()
        
        if let errorObject = try? decoder.decode(CaptchaErrorData.self, from: data), errorObject.code != ResponseCode.success.rawValue, errorObject.code != ResponseCode.bypass.rawValue{
            complete(.failure(CaptchaFail(code: errorObject.code, message: errorObject.message)))
            return
        }
        
        guard let object = try? decoder.decode(returnType.self, from: data) else {
            return
        }
        
        complete(.success(object))
    }
}
