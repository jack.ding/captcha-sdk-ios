//
//  CaptchaViewControllerBuilder.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/10/2.
//

import UIKit

public class CaptchaViewControllerBuilder{
    var alertWindow: UIWindow?
    var customViewController: UIViewController?
    private var uuid: String?
    private var captchaCallback: CaptchaCallback?

    public init(){
    
    }

    func setupAlertWindow( alertWindow: UIWindow) -> Self{
        self.alertWindow = alertWindow
        return self
    }

    func setupCustomView( customViewController: UIViewController) -> Self{
        self.customViewController = customViewController
        return self
    }

    func buildWindow() -> UIWindow? {
        alertWindow?.backgroundColor = .clear
        alertWindow?.rootViewController = customViewController
        guard let alertWindow = alertWindow else { return nil }
        return alertWindow
    }
    

    public func setupUuid( uuid: String) -> Self{
        self.uuid = uuid
        return self
    }

    public func setupCaptchaCallback( captchaCallback: CaptchaCallback?) -> Self{
        self.captchaCallback = captchaCallback
        return self
    }

    public func build() -> CaptchaViewController? {
        guard let uuid = uuid, let captchaCallback = captchaCallback else {
            return nil
        }
        return CaptchaViewController(uuid: uuid, captchaCallback: captchaCallback)
    }

}

@available(iOS 13.0, *)
class SubCaptchaViewControllerBuilder: CaptchaViewControllerBuilder{
    private var windowScene: UIWindowScene?

    override init(){
        super.init()
    }

    func setWindowScene( windowScene: UIWindowScene?) -> Self{
        self.windowScene = windowScene
        return self
    }

    override public func buildWindow() -> UIWindow? {
        alertWindow?.windowScene = windowScene
        alertWindow?.backgroundColor = .clear
        alertWindow?.rootViewController = customViewController
        guard let alertWindow = alertWindow else { return nil }
        return alertWindow
    }
}

