//
//  CaptchaViewController.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/10/1.
//

import UIKit

protocol CaptchaViewProtocol: UIView{
    var uuid: String {get set}
    var captchaCallback: CaptchaCallback {get set}
    var delegate: CaptchaDialogViewDelegate? {get set}
    func show()
    func showAfterPrepared(callback: @escaping (Result<ChartInfoResponseData, CaptchaFail>) -> Void)
}

public class CaptchaViewController: UIViewController {
    let uuid:String
    let captchaCallback: CaptchaCallback
    private var alertWindow: UIWindow = UIWindow()
    var captchaDialogView: CaptchaViewProtocol!

    @available(iOS 13.0, *)
    lazy var windowScene: UIWindowScene? = {
        let windowScene: UIWindowScene?
        windowScene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene
        return windowScene
    }()

    init(uuid:String, captchaCallback: @escaping CaptchaCallback) {
        self.uuid = uuid
        self.captchaCallback = captchaCallback
        super.init(nibName: nil, bundle: nil)
        //setup Captcha Dialog View
        self.captchaDialogView = CaptchaDialogBuilder().setupUuid(uuid: uuid).setupCaptchaCallback(captchaCallback: captchaCallback).build()
        self.captchaDialogView.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        //add your code here
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

// internal
extension CaptchaViewController{
    private func setupUI() {
        //setup BackGroundColor
        view.backgroundColor = ColorPatten.alertBackground.color
        view.addSubview(captchaDialogView)
        captchaDialogView.translatesAutoresizingMaskIntoConstraints = false
        captchaDialogView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        captchaDialogView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

// public
extension CaptchaViewController {
    public func showAfterPrepared(){
        captchaDialogView.showAfterPrepared { (result) in
            switch result{
            case .success( _):
                self.setupUI()
                if #available(iOS 13.0, *) {
                    guard let alertWindow = SubCaptchaViewControllerBuilder().setWindowScene(windowScene: self.windowScene).setupAlertWindow(alertWindow: self.alertWindow).setupCustomView(customViewController: self).buildWindow() else {return}
                    self.alertWindow = alertWindow
                } else {
                    guard let alertWindow = CaptchaViewControllerBuilder().setupAlertWindow(alertWindow: self.alertWindow).setupCustomView(customViewController: self).buildWindow() else {return}
                    self.alertWindow = alertWindow
                }
                self.alertWindow.isHidden = false
            case .failure(let error):
                self.captchaCallback(.failure(error))
            }
        }
    }
    
    public func show(){
        setupUI()
        captchaDialogView.show()
        if #available(iOS 13.0, *) {
            guard let alertWindow = SubCaptchaViewControllerBuilder().setWindowScene(windowScene: windowScene).setupAlertWindow(alertWindow: alertWindow).setupCustomView(customViewController: self).buildWindow() else {return}
            self.alertWindow = alertWindow
        } else {
            guard let alertWindow = CaptchaViewControllerBuilder().setupAlertWindow(alertWindow: self.alertWindow).setupCustomView(customViewController: self).buildWindow() else {return}
            self.alertWindow = alertWindow
        }

        alertWindow.isHidden = false
    }
}

// implement CaptchaDialogViewDelegate
extension CaptchaViewController: CaptchaDialogViewDelegate{
    public func cancelCaptchaDialogView() {
        DispatchQueue.main.async {
            self.alertWindow.isHidden = true
        }
    }
}

