//
//  Define.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/6.
//

import UIKit
import AdSupport

public typealias CaptchaCallback = (Result<Int?, CaptchaFail>) -> Void
public typealias ApiCallback = (Result<Int?, CaptchaFail>) -> Void

enum CapchaPopViewEventType {
    case close
    case refresh
    case info
}

enum ResponseCode: Int {
    case success = 10001
    case bypass = 10002
    case networkDisconnect = -1005
    case judgeMentRetriesExceeded = 63403
    case challengeExpiredOrNotExist = 63002
    case refreshChartExceeded = 63301
}

public struct CaptchaFail: Error{
    public let code: Int
    public let message: String
}

enum HttpMethod {
    case Post( Method: String = "POST", Data: Data?)
}

enum ColorPatten {
    case alertBackground
    case captchaSuccessText
    case captchaFailText
    case captchaSuccessStatusView
    case captchaViewFailStatusView
    
    var color: UIColor{
        switch self {
        case .alertBackground:
            return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        case .captchaSuccessText:
            return UIColor(hexString: "#2ca061")
        case .captchaFailText:
            return UIColor(hexString: "#b42f24")
        case .captchaSuccessStatusView:
            return UIColor(red: (194 / 255.0), green: (243 / 255.0), blue: (214 / 255.0), alpha: 0.8)
        case .captchaViewFailStatusView:
            return UIColor(red: (242 / 255.0), green: (174 / 255.0), blue: (170 / 255.0), alpha: 0.8)
        }
    }
}

struct LayoutConstraints {
    static var screenSize: CGSize {
        CGSize(width: min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), height: max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height))
    }
    static let challengeViewPaddingWidth: CGFloat = 12
    static let challengeViewPaddingHeigh: CGFloat = 41
    static let selfMarginViewWidth: CGFloat = 26
    static let selfWidth = LayoutConstraints.screenSize.width - LayoutConstraints.selfMarginViewWidth * 2
    static let challengeViewWidth = selfWidth - LayoutConstraints.challengeViewPaddingWidth * 2
}

struct Wording {
    static var abnormal: String {
        switch FunCaptcha.shared.language {
        case "zh":
            return "网路异常，请关闭后重新尝试"
        case "th":
            return "การเชื่อมต่อผิดปกติ กรุณาปิดและลองใหม่อีกครั้ง"
        case "vi":
            return "Lỗi hệ thống, vui lòng đóng và thử lại sau."
        case "in":
            return "Jaringan tidak normal, silakan tutup dan coba lagi."
        default:
            return "The network is abnormal, please try again."
        }
    }
}
