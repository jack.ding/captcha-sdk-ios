//
//  ExtensionFile.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/1.
//

import UIKit

//UIImageView
extension UIImageView{
    func load(queue: DispatchQueue, url: URL, complete: @escaping ((_ imageSize: CGSize) -> ())) {
        queue.async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        complete(image.size)
                    }
                }
            }else{
                complete(.zero)
            }
        }
    }
    
    func loadWithShuffleMatrix(queue: DispatchQueue, url: URL, shuffleMatrix: [Int], complete: @escaping ((_ imageSize: CGSize) -> ())){
        queue.async {
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        if (shuffleMatrix.count > 0){
                            let verticalCut = 10
                            let horizontalCut = 2
                            
                            let imageWidth = image.size.width
                            let imageHeight = image.size.height
                            let imageWidthRange = imageWidth / CGFloat(verticalCut)
                            let imageHeightRange = imageHeight / CGFloat(horizontalCut)
                            
                            // get origin point list
                            var originPointArray = [CGPoint]()
                            for y in 0...horizontalCut-1{
                                for x in 0...verticalCut-1{
                                    let startX = CGFloat(x) * imageWidthRange
                                    let startY = CGFloat(y) * imageHeightRange
                                    originPointArray.append(CGPoint(x:startX, y:startY))
                                }
                            }
                            
                            UIGraphicsBeginImageContext(CGSize(width: imageWidth, height: imageHeight))
                            var indexCount = 0;
                           
                            // recover image
                            for y in 0...horizontalCut-1{
                                for x in 0...verticalCut-1{
                                    
                                    let startX = CGFloat(x) * imageWidthRange;
                                    let startY = CGFloat(y) * imageHeightRange;
                                    let originPoint = originPointArray[shuffleMatrix[indexCount]]
                                    
                                    //
                                    let newImageCGRect = CGRect(x: originPoint.x, y: originPoint.y, width: imageWidthRange, height: imageHeightRange)
                                    
                                    let oriImageCGRect = CGRect(x: startX, y: startY, width: imageWidthRange, height: imageHeightRange)

                                    guard let cutImageRef: CGImage = image.cgImage?.cropping(to:oriImageCGRect)
                                    else {
                                        return
                                    }
                                    let croppedImage: UIImage = UIImage(cgImage: cutImageRef)
                                    croppedImage.draw(in: newImageCGRect)
                                    indexCount+=1
                                }
                            }
                            let newImage = UIGraphicsGetImageFromCurrentImageContext()
                            UIGraphicsEndImageContext()
                            self.image = newImage
                        }
                        else{
                            self.image = image
                        }
                        complete(image.size)
                    }
                }
            }else{
                complete(.zero)
            }
        }
    }
}

extension String{
     func verifyUrl() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: self)!)
    }
}

extension CGFloat{
    func integerValue() -> Int {Int(self)}
}

extension Data{
    func dataToString() {
        print("""
            //////////////////////////////
            \(String(data: self, encoding: .utf8) ?? ""))
            //////////////////////////////
            """)
    }
}

extension NSLayoutAnchor {
    @objc func constraintEqualToAnchor(anchor: NSLayoutAnchor!, constant:CGFloat, identifier:String) -> NSLayoutConstraint! {
        let constraint = self.constraint(equalTo: anchor, constant:constant)
        constraint.identifier = identifier
        return constraint
    }
}

extension UIView {
    func constraint(withIdentifier:String) -> NSLayoutConstraint? {
        return self.constraints.filter{ $0.identifier == withIdentifier }.first
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UITextView {
    func alignTextVerticallyInContainer() {
        var topCorrect = (self.bounds.size.height - self.contentSize.height * self.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect;
        self.contentInset.top = topCorrect
    }
}
