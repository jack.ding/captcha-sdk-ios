//
//  CaptchaDialogViewModel.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/28.
//

import UIKit

class CaptchaDialogViewModel{
    var uuid: String
    static var challengeInfo: ChartInfoResponseData?
    var captchaWording: CaptchaWordingProtocol?{
        CaptchaDialogViewModel.challengeInfo?.currentLocale
    }
    var captchaViewData: PuzzleViewProtocol?
    var puzzleImageView: UIImageView = UIImageView()
    var templateImageView: UIImageView = UIImageView()
    var puzzleImageSize: CGSize = .zero
    var templateImageSize: CGSize = .zero
//    let loadImageCount: Int = 3
    var captchaCallback: CaptchaCallback
    private var startTime: TimeInterval = Date().timeIntervalSince1970
    var ratio: CGFloat = 1.0

    init(uuid: String, captchaCallback: @escaping CaptchaCallback) {
        self.uuid = uuid
        self.captchaCallback = captchaCallback
    }
}

//MARK: 
extension CaptchaDialogViewModel{
    public func materialsPrepared(callback:  @escaping (Result<ChartInfoResponseData, CaptchaFail>) -> Void) {
        let request: ChartInfoRequestData = ChartInfoRequestData(challengeUuid: uuid, device: FunCaptcha.shared.device)
        CaptchaAPI.getChartInfo(request: request) { [weak self] (result) in
            guard let `self` = self else {return}
            switch result{
            case .success(let object):
                if let code = object?.code, code == ResponseCode.bypass.rawValue {
                    `self`.captchaCallback(.success(code))
                    return
                }
                CaptchaDialogViewModel.challengeInfo = object
                `self`.captchaViewData = object
                `self`.retryLogic(code: object?.code ?? ResponseCode.success.rawValue, callback: {
                    callback(.success(object!))
                })
            case .failure(let error):
                `self`.captchaCallback(.failure(CaptchaFail(code: error.code, message: error.message)))
            }
        }
    }
}

extension CaptchaDialogViewModel{
    private func retryLogic(code: Int, callback: (() -> Void)?) {
        let loadImageGroup: DispatchGroup = DispatchGroup()
        let queue = DispatchQueue(label: "com.loadImage.captcha")
        guard let chartURI = self.captchaViewData?.chartURI, let keyURI = self.captchaViewData?.keyURI else { return }
        loadImageGroup.enter()
        queue.async(qos: .default, flags: .barrier) {
            self.puzzleImageView.loadWithShuffleMatrix(queue: queue, url: URL(string: chartURI)!, shuffleMatrix: self.captchaViewData?.shuffleMatrix ?? []) {imageSize in
                self.puzzleImageSize = imageSize
                loadImageGroup.leave()
            }
        }
        loadImageGroup.enter()
        queue.async(qos: .default, flags: .barrier) { [unowned self] in
            templateImageView.load(queue: queue, url: URL(string: keyURI)!) {imageSize in
                self.templateImageSize = imageSize
                loadImageGroup.leave()
            }
        }

        loadImageGroup.notify(queue: .main) {
            if code == ResponseCode.success.rawValue{
                if self.puzzleImageSize == .zero || self.templateImageSize == .zero{
                    self.retryLogic(code: ResponseCode.success.rawValue, callback: nil)
                }else{
                    callback?()
                }
            }else if code == ResponseCode.judgeMentRetriesExceeded.rawValue{
                self.refreshChart(callback: nil)
            }
        }
    }
    
    func refreshChart(callback: ((Result<RefreshChartResponseData, CaptchaFail>) -> Void)?) {
        let request: ChartInfoRequestData = ChartInfoRequestData(challengeUuid: uuid, device: FunCaptcha.shared.device)
        CaptchaAPI.refreshChart(request: request) { [weak self] (result) in
            guard let `self` = self else {return}
            switch result{
            case .success(let object):
                if let code = object?.code, code == ResponseCode.bypass.rawValue{
                    `self`.captchaCallback(.success(code))
                    return
                }
                self.captchaViewData = object
                `self`.retryLogic(code: object?.code ?? ResponseCode.success.rawValue, callback: {
                    callback?(.success(object!))
                })
            case .failure(let error):
                switch error.code {
                case ResponseCode.challengeExpiredOrNotExist.rawValue, ResponseCode.refreshChartExceeded.rawValue:
                    callback?(.failure(error))
                default:
                    callback?(.failure(error))
                }
                `self`.captchaCallback(.failure(CaptchaFail(code: error.code, message: error.message)))
            }
        }
    }
    
    func getPuzzleHeight(popViewWidth: CGFloat) -> CGFloat {
        self.puzzleImageSize.width == 0 ? 0 : (popViewWidth / self.puzzleImageSize.width) * self.puzzleImageSize.height
    }
    
    func getPuzzleImageScaleRatio(popViewWidth: CGFloat) -> CGFloat {
        ratio = self.puzzleImageSize.width == 0 ? 1 : popViewWidth / self.puzzleImageSize.width
        return ratio
    }
    
    func jugdementAnswer(location: CGPoint, cost: Double, completed: @escaping (Result<JudgementResponseData?, CaptchaFail>) -> Void){
        let recoveredX = location.x / ratio
        let recoveredY = location.y / ratio
        print("My Cost: \(cost)")
        
        let request: JudgementRequestData = JudgementRequestData(challengeUUID: uuid, activity: JudgementRequestData.Activity(answers: JudgementRequestData.Answers(x: recoveredX.integerValue(), y: recoveredY.integerValue()), cost: cost), device: JudgementRequestData.Device(ip: nil, language: nil, userAgent: nil, uuid: nil, domain: nil))
        CaptchaAPI.judgeAnswer(request: request) {(result) in
            switch result{
            case .success(let object):
                completed(.success(object))
            case .failure(let error):
                completed(.failure(error))
            }
        }
    }
}
