//
//  CaptchaBuilder.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/10/2.
//

import UIKit

class CaptchaDialogBuilder {
    private var uuid: String?
    private var captchaCallback: CaptchaCallback?

    init(){
    
    }
    
    func setupUuid( uuid: String) -> Self{
        self.uuid = uuid
        return self
    }

    func setupCaptchaCallback( captchaCallback: CaptchaCallback?) -> Self{
        self.captchaCallback = captchaCallback
        return self
    }
    
    func build() -> CaptchaDialogView? {
        guard let uuid = uuid else { return nil }
        return CaptchaDialogView(uuid: uuid, captchaCallback: captchaCallback!)
    }
}
