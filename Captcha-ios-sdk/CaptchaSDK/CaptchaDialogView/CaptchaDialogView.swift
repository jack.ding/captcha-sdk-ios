//
//  CaptchaDialogView.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/28.
//

import UIKit

protocol CaptchaDialogViewDelegate: AnyObject {
    func cancelCaptchaDialogView()
}

public class CaptchaDialogView: UIView, CaptchaViewProtocol {
    var uuid: String
    private var captchaViewHeight: NSLayoutConstraint?
    var captchaCallback: CaptchaCallback
    weak var delegate: CaptchaDialogViewDelegate?
    private let viewModel: CaptchaDialogViewModel
    private var captchaView: CaptchaView

    lazy private var notedLabel: UILabel = {
        let notedLabel = UILabel()
        notedLabel.numberOfLines = 0
        notedLabel.translatesAutoresizingMaskIntoConstraints = false
        notedLabel.textColor = .black
        notedLabel.font = UIFont.systemFont(ofSize: 14)
        return notedLabel
    }()

    lazy private var closeButton: UIButton = {
        let closeButton = UIButton()
        let image = UIImage(named: "Delete", in: Bundle(for: type(of: self)), compatibleWith: nil)
        closeButton.setImage(image, for: .normal)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        return closeButton
    }()
    
    lazy private var refreshButton: UIButton = {
        let refreshButton = UIButton()
        let image = UIImage(named: "Refresh", in: Bundle(for: type(of: self)), compatibleWith: nil)
        refreshButton.setImage(image, for: .normal)
        refreshButton.translatesAutoresizingMaskIntoConstraints = false
        refreshButton.addTarget(self, action: #selector(refreshButtonClicked), for: .touchUpInside)
        return refreshButton
    }()
    
    lazy private var noteButton: UIButton = {
        let noteButton = UIButton()
        let image = UIImage(named: "Info", in: Bundle(for: type(of: self)), compatibleWith: nil)
        noteButton.setImage(image, for: .normal)
        noteButton.translatesAutoresizingMaskIntoConstraints = false
        noteButton.addTarget(self, action: #selector(noteButtonClicked), for: .touchUpInside)
        return noteButton
    }()
    
    lazy private var closeDescriptionButton: UIButton = {
        let closeDescriptionButton = UIButton()
        let image = UIImage(named: "Delete-1", in: Bundle(for: type(of: self)), compatibleWith: nil)
        closeDescriptionButton.setImage(image, for: .normal)
        closeDescriptionButton.translatesAutoresizingMaskIntoConstraints = false
        closeDescriptionButton.addTarget(self, action: #selector(closeDescriptionButtonClicked), for: .touchUpInside)
        return closeDescriptionButton
    }()
    
    lazy private var corporationIncLabel: UILabel = {
        let corporationIncLabel = UILabel()
        corporationIncLabel.numberOfLines = 0
        corporationIncLabel.textAlignment = .right
        corporationIncLabel.textColor = .lightGray
        corporationIncLabel.translatesAutoresizingMaskIntoConstraints = false
        corporationIncLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
        return corporationIncLabel
    }()
    
    lazy private var templateImageView: UIImageView = {
        let templateImageView = UIImageView()
        templateImageView.contentMode = .scaleAspectFit
        return templateImageView
    }()
    
    lazy private var puzzleImageView: UIImageView = {
        let puzzleImageView = UIImageView()
        puzzleImageView.contentMode = .scaleAspectFit
        return puzzleImageView
    }()
    
    lazy private var descriptionView: UIView = {
        let descriptionView = UIView()
        descriptionView.backgroundColor = UIColor.black
        descriptionView.alpha = 0.7
        descriptionView.translatesAutoresizingMaskIntoConstraints = false
        return descriptionView
    }()

    lazy private var descriptionTextView: UITextView = {
        let descriptionTextView = UITextView()
        descriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        descriptionTextView.backgroundColor = .clear
        descriptionTextView.isScrollEnabled = false
        descriptionTextView.isEditable = false
        descriptionTextView.textAlignment = .left
        descriptionTextView.font = UIFont(name: "NotoSansSC-Regular", size: 14)
        descriptionTextView.textColor = .white
        return descriptionTextView
    }()
    
    public init(uuid: String, captchaCallback: @escaping CaptchaCallback) {
        self.uuid = uuid
        self.captchaCallback = captchaCallback
        self.viewModel = CaptchaDialogViewModel(uuid: uuid, captchaCallback: captchaCallback)
        captchaView = CaptchaView()
        super.init(frame: .zero)
        captchaView.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: public interface
extension CaptchaDialogView{
    ///Materials prepared done then display dialog view
    public func showAfterPrepared(callback: @escaping (Result<ChartInfoResponseData, CaptchaFail>) -> Void) {
        self.viewModel.materialsPrepared {[weak self] (result) in
            guard let `self` = self else {return}
            switch result{
            case .success(let object):
                `self`.setupLayout()
                callback(.success(object))
            case .failure(_): break
            }
        }
    }

    ///Display dialog view immediately
    public func show(){
        self.setupLayout()
        self.viewModel.materialsPrepared {[weak self] (result) in
            guard let `self` = self else{return}
            switch result{
            case .success(_):
                `self`.updateCaptchaView(captchaViewStatus: .showChart)
            case .failure(_): break
            }
        }
    }

    private func setupLayout() {
        let challengeViewHeight = self.viewModel.getPuzzleHeight(popViewWidth: LayoutConstraints.challengeViewWidth)
        captchaView.contentMode = .scaleAspectFit
        captchaView.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(notedLabel)
        self.addSubview(closeButton)
        self.addSubview(refreshButton)
        self.addSubview(noteButton)
        self.addSubview(corporationIncLabel)
        self.addSubview(captchaView)

        // setup self layer
        self.layer.cornerRadius = 7.0
        self.backgroundColor = .white
        // setup notedLabel constraints
        notedLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        notedLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        notedLabel.trailingAnchor.constraint(equalTo: closeButton.leadingAnchor, constant: -12).isActive = true
        // setup closeButton constraints
        closeButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        closeButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        // setup refreshButton constraints
        refreshButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        refreshButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        refreshButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        refreshButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        // setup tutorButton constraints
        noteButton.leadingAnchor.constraint(equalTo: refreshButton.trailingAnchor, constant: 12).isActive = true
        noteButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        noteButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        noteButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        // setup corporationIncLabel constraints
        corporationIncLabel.leadingAnchor.constraint(equalTo: noteButton.trailingAnchor, constant: 12).isActive = true
        corporationIncLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        corporationIncLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        // setup notedLabel constraints
        captchaView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        captchaView.topAnchor.constraint(equalTo: notedLabel.bottomAnchor, constant: 12).isActive = true
        captchaView.bottomAnchor.constraint(equalTo: refreshButton.topAnchor, constant: -12).isActive = true
        captchaView.widthAnchor.constraint(equalToConstant: LayoutConstraints.challengeViewWidth).isActive = true
        captchaViewHeight = captchaView.heightAnchor.constraint(equalToConstant: challengeViewHeight)
        captchaViewHeight?.isActive = true
        captchaView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        updateCaptchaView(captchaViewStatus: .showChart)
    }
    
    private func updateCaptchaView(captchaViewStatus: CaptchaView.ChartViewStatus) {
        captchaView.setupChartViewStatus(chartViewStatus: captchaViewStatus)
        switch captchaViewStatus {
        case .showChart:
            let puzzleScaleRatio = self.viewModel.getPuzzleImageScaleRatio(popViewWidth: LayoutConstraints.challengeViewWidth)
            captchaViewHeight?.constant = self.viewModel.getPuzzleHeight(popViewWidth: LayoutConstraints.challengeViewWidth)
            captchaView.layoutUI(ratio: puzzleScaleRatio, puzzleImage: self.viewModel.puzzleImageView.image ?? UIImage(), templateImage: self.viewModel.templateImageView.image ?? UIImage())
            notedLabel.text = viewModel.captchaWording?.info
            corporationIncLabel.text = viewModel.captchaWording?.provide
            descriptionTextView.text = viewModel.captchaWording?.currentLocaleDescription
            captchaView.setupChartViewStatus(chartViewStatus: .showChart)
        default: break
        }
    }
    
    private func addNoteView() {
        captchaView.addSubview(descriptionView)
        descriptionView.topAnchor.constraint(equalTo: captchaView.topAnchor).isActive = true
        descriptionView.bottomAnchor.constraint(equalTo: captchaView.bottomAnchor).isActive = true
        descriptionView.leadingAnchor.constraint(equalTo: captchaView.leadingAnchor).isActive = true
        descriptionView.trailingAnchor.constraint(equalTo: captchaView.trailingAnchor).isActive = true
        
        //setup descriptionTextView
        descriptionView.addSubview(descriptionTextView)
        descriptionTextView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor).isActive = true
        descriptionTextView.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor).isActive = true
        descriptionTextView.centerYAnchor.constraint(equalTo: descriptionView.centerYAnchor).isActive = true

        //setup descriptionTextView
        descriptionView.addSubview(closeDescriptionButton)
        closeDescriptionButton.topAnchor.constraint(equalTo: descriptionView.topAnchor, constant: 10).isActive = true
        closeDescriptionButton.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor, constant: -10).isActive = true
        closeDescriptionButton.heightAnchor.constraint(equalToConstant: 16).isActive = true
        closeDescriptionButton.widthAnchor.constraint(equalToConstant: 16).isActive = true
    }
    
    private func closeDescriptionView() {
        descriptionView.removeFromSuperview()
    }
}

///Button action event
extension CaptchaDialogView{
    @objc private func closeButtonClicked() {
        self.delegate?.cancelCaptchaDialogView()
    }

    @objc private func closeDescriptionButtonClicked() {
        self.closeDescriptionView()
    }

    @objc private func refreshButtonClicked() {
        self.updateCaptchaView(captchaViewStatus: .refresh)
        self.closeDescriptionView()
        self.viewModel.refreshChart { [weak self] (result) in
            guard let `self` = self else { return}
            switch result {
            case .success(let object):
                if object.code == ResponseCode.success.rawValue {
                    `self`.updateCaptchaView(captchaViewStatus: .showChart)
                }
            case .failure(let error):
                switch error.code {
                case Int.min ... ResponseCode.networkDisconnect.rawValue:
                    `self`.updateCaptchaView(captchaViewStatus: .networkFail)
                default:
                    `self`.updateCaptchaView(captchaViewStatus: .refresh)
                    `self`.captchaView.setupIssueLabelMessage(message: error.message)
                }
            }
        }
    }
    
    @objc private func noteButtonClicked() {
        addNoteView()
    }
}

extension CaptchaDialogView: ChallengViewDelegate{
    func captchaView(captchaView: CaptchaView, onAction: CGPoint, cost: Double) {
        self.viewModel.jugdementAnswer(location: onAction, cost: cost) {[weak self] (result) in
            guard let `self` = self else {return}
            switch result{
            case .success(let object):
                `self`.captchaView.judgeResultAnimate(result: .success(object), completed: nil)
                `self`.captchaCallback(.success(object?.code))
            case .failure(let error):
                switch error.code {
                case Int.min ... ResponseCode.networkDisconnect.rawValue:
                    `self`.updateCaptchaView(captchaViewStatus: .networkFail)
                default:
                    `self`.captchaView.judgeResultAnimate(result: .failure(error)) {
                        if error.code == ResponseCode.judgeMentRetriesExceeded.rawValue || error.code == ResponseCode.challengeExpiredOrNotExist.rawValue{
                            `self`.refreshButtonClicked()
                        }
                    }
                }
                `self`.captchaCallback(.failure(CaptchaFail(code: error.code, message: error.message)))
            }
        }
    }
}

