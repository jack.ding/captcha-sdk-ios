//
//  Captcha.swift
//  FPCaptcha
//
//  Created by Jack Din on 2021/9/27.
//

import Foundation

public class FunCaptcha {
    
    public static let shared: FunCaptcha = FunCaptcha()
    
    public enum ServerType{
        case dev
        case stage
        case softlaunch
        case production
    }
    
    var device: Device = Device(language: nil, ip: nil, userAgent: nil, domain: nil)
    var language: String
    var serverType: ServerType = .stage
    
    init() {
        self.language = "en"
    }
        
    public func config(serverType: ServerType, language: String?, device: Device){
        self.device = device
        self.serverType = serverType
        if let language = language {
            self.language = language
        }
    }
}


