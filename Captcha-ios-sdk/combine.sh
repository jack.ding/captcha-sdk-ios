#!/bin/sh

# iOS universal library build script supporting swift modules inclusive simulator slices
#make output folder

UniversalFrameworkPath="./UniversalFramework"
DERIVED_DATA_PATH=${UniversalFrameworkPath}/DERIVED_DATA
DSTROOT=${UniversalFrameworkPath}/DSTROOT

BIN_FILES_PATH=""

echo "UniversalFrameworkPath:" $UniversalFrameworkPath
echo "BUILD_PATH:" $BUILD_PATH
echo "DSTROOT:" $DSTROOT

rm -rf ${UniversalFrameworkPath}> /dev/null
mkdir -p ${UniversalFrameworkPath}

#ENVS ARCH:    those two are paired,
#              which menas iphoneos<->arm64 iphonesimulator<->x86_64
ENVS=(iphoneos iphonesimulator)
ARCH=(arm64 x86_64)

xcodebuild clean

for ((i=0;i<${#ENVS[@]};i++))
do
echo $env
xcodebuild \
-scheme FPCaptcha \
-sdk ${ENVS[i]} \
-configuration release \
-arch ${ARCH[i]} \
DSTROOT=${DSTROOT} \
SKIP_INSTALL=NO \
INSTALL_PATH="/${ENVS[i]}" \
ONLY_ACTIVE_ARCH=NO \
install

cp -rf "${DSTROOT}/${ENVS[i]}/FPCaptcha.framework" "${UniversalFrameworkPath}"

#BIN_FILES_PATH:concat the possible bin file path, used for lipo
BIN_FILES_PATH="${BIN_FILES_PATH} ${DSTROOT}/${ENVS[i]}/FPCaptcha.framework/FPCaptcha"

done

echo "BIN_FILES_PATH:" $BIN_FILES_PATH

lipo -create \
$BIN_FILES_PATH \
-output "${UniversalFrameworkPath}/FPCaptcha.framework/FPCaptcha"

rm -rf $DSTROOT

# No Need Zip anymore, gitlab don't allow, too big.
#cd ${UniversalFrameworkPath}
#zip -r "FunpodiumSDK.framework.zip" "FunpodiumSDK.framework"

exit 0
